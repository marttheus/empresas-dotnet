using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace API_IMDb.Tests
{
    [CollectionDefinition("Integration Tests")]
    public class TestCollection : ICollectionFixture<WebApplicationFactory<API_IMDb.Startup>>
    {
        
    }
}