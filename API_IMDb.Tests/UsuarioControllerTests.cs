﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace API_IMDb.Tests
{
    [Collection("Integration Tests")]
    public class UsuarioControllerTests
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public UsuarioControllerTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetRoot_ReturnsSuccessAndStatusUp()
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync("/Usuario");

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.NotNull(response.Content);
        }
    }
}