﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace API_IMDb.Models
{
    public class Genero
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Filme> Filmes { get; set; }
    }
}
