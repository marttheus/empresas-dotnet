﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace API_IMDb.Models
{
    public class ArtistaFilme
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int idArtista { get; set; }
        [Required]
        public int idFilme { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("idFilme")]
        public virtual Filme Filme { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("idArtista")]
        public virtual Artista Artista { get; set; }
    }
}
