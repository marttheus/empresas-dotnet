﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace API_IMDb.Models
{
    public class IMDbContext : DbContext
    {
        public IMDbContext()
        {
        }

        public IMDbContext(DbContextOptions<IMDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // for the other conventions, we do a metadata model loop
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                // equivalent of modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
                // and modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
                entityType.GetForeignKeys()
                    .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade)
                    .ToList()
                    .ForEach(fk => fk.DeleteBehavior = DeleteBehavior.Restrict);
            }

            modelBuilder.Entity<Usuario>().HasData(new Usuario[] {
                 new Usuario{ id = 1, email="adm@imdb.com", nome = "Administrador", senha = "654321", ativo = 1, perfil = "administrador" },
                 new Usuario{ id = 2, email="usuario@imdb.com", nome = "Padrão", senha = "123456", ativo = 1, perfil = "usuario"},
            });

            modelBuilder.Entity<Genero>().HasData(new Genero[] {
                 new Genero{ id = 1, nome = "Crime" }
            });

            modelBuilder.Entity<Artista>().HasData(new Artista[] {
                 new Artista{ id = 1, nome = "Quentin Tarantino", ano_nascimento = new DateTime(1963, 3, 27) },
                 new Artista{ id = 2, nome = "John Travolta", ano_nascimento = new DateTime(1954, 2, 18) },
                 new Artista{ id = 3, nome = "Uma Thurman", ano_nascimento = new DateTime(1970, 4, 29) },
                 new Artista{ id = 4, nome = "Samuel L. Jackson", ano_nascimento = new DateTime(1948, 12, 21) }
            });

            modelBuilder.Entity<Filme>().HasData(new Filme[] {
                 new Filme{ id = 1, nome = "Pulp Fiction", ano_lancamento = new DateTime(1994, 3, 3), idDiretor = 1, idGenero = 1 }
            });

            modelBuilder.Entity<ArtistaFilme>().HasData(new ArtistaFilme[] {
                 new ArtistaFilme{ id = 1, idArtista = 2, idFilme = 1 },
                 new ArtistaFilme{ id = 2, idArtista = 3, idFilme = 1 },
                 new ArtistaFilme{ id = 3, idArtista = 4, idFilme = 1 }
            });

            modelBuilder.Entity<VotoUsuario>().HasData(new VotoUsuario[] {
                 new VotoUsuario{ id = 1, idUsuario = 2, idFilme = 1, voto = 4 },
                 new VotoUsuario{ id = 2, idUsuario = 2, idFilme = 1, voto = 3 },
                 new VotoUsuario{ id = 3, idUsuario = 2, idFilme = 1, voto = 2 }
            });
        }


        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Filme> Filme { get; set; }
        public DbSet<Artista> Artista { get; set; }
        public DbSet<VotoUsuario> VotoUsuario { get; set; }
        public DbSet<Genero> Genero { get; set; }
        public DbSet<ArtistaFilme> ArtistaFilme { get; set; }
    }
}
