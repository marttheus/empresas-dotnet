﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace API_IMDb.Models
{
    public class VotoUsuario
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int idUsuario { get; set; }
        [Required]
        public int idFilme { get; set; }
        [Required]
        [Range(1, 4)]
        public int voto { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("idFilme")]
        public virtual Filme Filme { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("idUsuario")]
        public virtual Usuario Usuario { get; set; }
    }
}
