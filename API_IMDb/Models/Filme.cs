﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_IMDb.Models
{
    public class Filme
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string nome { get; set; }
        public DateTime? ano_lancamento { get; set; }
        public int idDiretor { get; set; }
        public int idGenero { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("idDiretor")]
        public virtual Artista Diretor { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("idGenero")]
        public virtual Genero Genero { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<VotoUsuario> Votos { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<ArtistaFilme> ArtistaFilmes { get; set; }
    }
}
