﻿using API_IMDb.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace API_IMDb.Models
{
    public class Usuario
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string nome { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [Required]
        public string senha { get; set; }
        public int ativo { get; set; }
        public string perfil { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<VotoUsuario> Votos { get; set; }

        public Usuario()
        {
        }

        public Usuario (UsuarioDTO.Cadastro usuario, string _perfil = "usuario")
        {
            email = usuario.email;
            nome = usuario.nome;
            senha = usuario.senha;
            ativo = 1;
            perfil = _perfil;
        }
    }
}
