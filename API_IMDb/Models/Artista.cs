﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace API_IMDb.Models
{
    public class Artista
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string nome { get; set; }
        public DateTime? ano_nascimento { get; set; }


        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Filme> Filmes { get; set; }
    }
}
