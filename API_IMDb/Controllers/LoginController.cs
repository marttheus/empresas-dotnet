﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_IMDb.Models;
using Microsoft.AspNetCore.Authorization;
using API_IMDb.DTO;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;

namespace API_IMDb.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IMDbContext _context;
        private readonly IConfiguration _config;

        public LoginController(IMDbContext context, IConfiguration configuration)
        {
            _context = context;
            _config = configuration;
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login([FromBody] UsuarioDTO.Login login)
        {
            if (ModelState.IsValid)
            {
                var usuario = _context.Usuario.Where(w => w.email == login.email).FirstOrDefault();

                if(usuario != null)
                {
                    var token = GenerateJWT(usuario.email, usuario.perfil);

                    return Ok(new { token = token });
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        private string GenerateJWT(string email, string role)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["TokenConfigurations:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Email, email),
                new Claim("perfil", role)
            };

            var token = new JwtSecurityToken(
                issuer: _config["TokenConfigurations:Issuer"],
                audience: _config["TokenConfigurations:Audience"],
                claims,
                expires: DateTime.Now.AddSeconds(Convert.ToDouble(_config["TokenConfigurations:Seconds"])),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
