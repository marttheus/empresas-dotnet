﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_IMDb.Models;
using Microsoft.Extensions.Configuration;
using API_IMDb.DTO;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Security.Claims;

namespace API_IMDb.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IMDbContext _context;

        public UsuarioController(IMDbContext context)
        {
            _context = context;
        }

        // CADASTRO DE USUARIO
        // POST: api/Usuarios
        [HttpPost]
        public async Task<ActionResult<UsuarioDTO.Retorno>> PostUsuario(UsuarioDTO.Cadastro cadastro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if(UsuarioExiste(cadastro.email))
            {
                return BadRequest(new { erro = "Email cadastrado previamente." });
            }

            var usuario = new Usuario(cadastro);

            _context.Usuario.Add(usuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsuario", new { id = usuario.id }, usuario);
        }

        // EDICAO DE USUARIO
        // PUT: api/Usuarios
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuario(int id, UsuarioDTO.Edicao edicao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var usuario = await _context.Usuario.Where(w => w.id == id).FirstOrDefaultAsync();

            if(usuario == null)
            {
                return NotFound();
            }

            usuario.nome = edicao.nome;
            usuario.senha = edicao.senha;

            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        // DESATIVACAO DE USUARIO
        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UsuarioDTO.Retorno>> DeleteUsuario(int id)
        {
            var usuario = await _context.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            usuario.ativo = 0;

            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return new UsuarioDTO.Retorno(usuario);
        }

        // GET: api/Usuarios/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("{id}")]
        public async Task<ActionResult<UsuarioDTO.Retorno>> GetUsuario(int id)
        {
            var usuario = await _context.Usuario.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return new UsuarioDTO.Retorno(usuario);
        }

        private bool UsuarioExiste(string email)
        {
            return _context.Usuario.Any(a => a.email == email);
        }
    }
}
