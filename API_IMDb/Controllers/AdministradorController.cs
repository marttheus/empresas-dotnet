﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_IMDb.Models;
using API_IMDb.DTO;
using Microsoft.AspNetCore.Authorization;

namespace API_IMDb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdministradorController : ControllerBase
    {
        private readonly IMDbContext _context;

        public AdministradorController(IMDbContext context)
        {
            _context = context;
        }

        // CADASTRO DE ADMINISTRADOR
        // POST: api/Administrador
        [HttpPost]
        public async Task<ActionResult<Usuario>> PostAdministrador(UsuarioDTO.Cadastro cadastro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (AdministradorExiste(cadastro.email))
            {
                return BadRequest(new { erro = "Email cadastrado previamente." });
            }

            var administrador = new Usuario(cadastro, "administrador");

            _context.Usuario.Add(administrador);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAdministrador", new { id = administrador.id }, administrador);
        }

        // EDICAO DE ADMINISTRADOR
        // PUT: api/Administrador
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdministrador(int id, UsuarioDTO.Edicao edicao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var administrador = await _context.Usuario.Where(w => w.id == id).FirstOrDefaultAsync();

            if (administrador == null)
            {
                return NotFound();
            }

            administrador.nome = edicao.nome;
            administrador.senha = edicao.senha;

            _context.Entry(administrador).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        // DESATIVACAO DE ADMINISTRADOR
        // DELETE: api/Administrador/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Usuario>> DeleteAdministrador(int id)
        {
            var administrador = await _context.Usuario.FindAsync(id);
            if (administrador == null)
            {
                return NotFound();
            }

            administrador.ativo = 0;

            _context.Entry(administrador).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return administrador;
        }

        // USUÁRIOS NÃO ADMINISTRADORES ATIVOS
        // DELETE: api/Administrador/UsuariosAtivos
        [HttpGet]
        [Route("UsuariosAtivos")]
        public async Task<ActionResult> GetUsuariosAtivo(int offset = 0, int limit = 50)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var total = await _context.Usuario
                .Where(w => w.perfil == "usuario" && w.ativo == 1)
                .CountAsync();

            var usuarios = await _context.Usuario
                .Where(w => w.perfil == "usuario" && w.ativo == 1)
                .OrderBy(o => o.nome)
                .Skip(offset)
                .Take(limit)
                .ToListAsync();

            var retorno = new
            {
                Usuarios = usuarios,
                Paginacao = new
                {
                    Total = total,
                    Limit = limit,
                    Offset = offset,
                    Returned = usuarios.Count()
                }
            };

            return Ok(retorno);
        }

        // GET: api/Administrador/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuario>> GetAdministrador(int id)
        {
            var administrador = await _context.Usuario.FindAsync(id);

            if (administrador == null)
            {
                return NotFound();
            }

            return administrador;
        }

        // GET: api/Administrador
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult<IEnumerable<Usuario>>> GetAdministrador()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            return await _context.Usuario.ToListAsync();
        }

        private bool AdministradorExiste(string email)
        {
            return _context.Usuario.Any(e => e.email == email);
        }
    }
}
