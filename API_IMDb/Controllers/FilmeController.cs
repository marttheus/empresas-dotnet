﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_IMDb.Models;
using API_IMDb.DTO;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Collections.Generic;

namespace API_IMDb.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FilmeController : ControllerBase
    {
        private readonly IMDbContext _context;

        public FilmeController(IMDbContext context)
        {
            _context = context;
        }

        // CADASTRO FILME - SOMENTE ADMINISTRADOR
        // POST: api/Filme
        [HttpPost]
        public async Task<ActionResult<FilmeDTO.Retorno>> PostFilme(FilmeDTO.Cadastro cadastro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (FilmeExiste(cadastro.nome))
            {
                return BadRequest(new { erro = "Filme cadastrado previamente." });
            }

            var administrador = UsuarioValido(HttpContext.User.Claims, "administrador");

            if (administrador == null)
            {
                return BadRequest(new { erro = "Administrador inválido ou inexistente." });
            }

            var filme = new Filme
            {
                nome = cadastro.nome,
                ano_lancamento = cadastro.ano_lancamento,
                idGenero = PrimaryKeyGenero(cadastro.genero),
                idDiretor = PrimaryKeyArtista(cadastro.diretor),
            };
            _context.Filme.Add(filme);

            foreach(var ator in cadastro.atores)
            {
                var artistaFilme = new ArtistaFilme
                {
                    idArtista = PrimaryKeyArtista(ator),
                    idFilme = filme.id
                };
                _context.ArtistaFilme.Add(artistaFilme);
            }

            await _context.SaveChangesAsync();

            return new ObjectResult(new FilmeDTO.Retorno(filme));
        }

        // GET: api/Filme/5
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FilmeDTO.Retorno>> GetFilme(int id)
        {
            var filme = await _context.Filme.FindAsync(id);

            if (filme == null)
            {
                return NotFound();
            }

            return new FilmeDTO.Retorno(filme);
        }

        // POST: api/Filme/Voto
        [HttpPost("{id}/Voto/{nota}")]
        public async Task<ActionResult> PostVoto(int id, int nota)
        {
            if (!FilmeExiste(id))
            {
                return NotFound();
            }

            if(nota < 1 || nota > 4)
            {
                return BadRequest("Nota menor que 1 ou maior que 4.");
            }

            var usuario = UsuarioValido(HttpContext.User.Claims);

            if (usuario == null)
            {
                return BadRequest(new { erro = "Usuário inválido ou inexistente." });
            }

            var voto = RecuperaVoto(id, usuario.id);

            if(voto == null)
            {
                voto = new VotoUsuario
                {
                    idFilme = id,
                    idUsuario = usuario.id,
                    voto = nota
                };

                _context.Entry<VotoUsuario>(voto).State = EntityState.Added;
            }
            else
            {
                voto.voto = nota;

                _context.Entry<VotoUsuario>(voto).State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();

            return Ok(new { mensagem = "Voto computado com sucesso!" });
        }

        // GET FILMES - PAGINACAO, FILTRO E ORDENACAO
        // GET: api/Filme
        [HttpGet]
        public async Task<ActionResult> GetFilme(string diretor, string nome, string genero, string ator, int offset = 0, int limit = 50)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var total = await _context.Filme
                .CountAsync();

            var query = _context.Filme
                .OrderByDescending(o => o.Votos.Select(s => s.voto).Average())
                .ThenBy(t => t.nome)
                .Skip(offset)
                .Take(limit);

            if (!string.IsNullOrEmpty(genero))
            {
                query = query.Where(w => w.Genero.nome == genero);
            }

            if(!string.IsNullOrEmpty(diretor))
            {
                query = query.Where(w => w.Diretor.nome == diretor);
            }

            if(!string.IsNullOrEmpty(nome))
            {
                query = query.Where(w => w.nome == nome);
            }

            if (!string.IsNullOrEmpty(ator))
            {
                query = query.Where(w => w.ArtistaFilmes.Select(s => s.Artista.nome).Contains(ator));
            }

            var filmes = await query.ToListAsync();

            var retorno = new
            {
                Filmes = filmes,
                Paginacao = new
                {
                    Total = total,
                    Limit = limit,
                    Offset = offset,
                    Returned = filmes.Count,
                }
            };

            return Ok(retorno);
        }

        [HttpGet]
        [Route("{id}/Detalhes")]
        public async Task<ActionResult<FilmeDTO.Datalhes>> GetFilmeDetalhados(int id)
        {
            var filme = await _context.Filme.Where(w => w.id == id)
                .Include(i => i.Diretor)
                .Include(i => i.ArtistaFilmes)
                .Include("ArtistaFilmes.Artista")
                .Include(i => i.Genero)
                .Include(i => i.Votos)
                .FirstOrDefaultAsync();

            if(filme == null)
            {
                return NotFound();
            }

            return new FilmeDTO.Datalhes(filme);
        }

        private bool FilmeExiste(string nome)
        {
            return _context.Filme.Any(a => a.nome == nome);
        }

        private bool FilmeExiste(int id)
        {
            return _context.Filme.Any(a => a.id == id);
        }

        private Usuario UsuarioValido(IEnumerable<Claim> claims, string perfil = "usuario")
        {
            var perfilUsuario = claims.Where(c => c.Type == "perfil").First().Value;
            var email = claims.Where(c => c.Type == ClaimTypes.Email).First().Value;

            return _context.Usuario.Where(w => w.email == email && perfil == perfilUsuario).FirstOrDefault();
        }

        private VotoUsuario RecuperaVoto(int idFilme, int idUsuario)
        {
            return _context.VotoUsuario.Where(a => a.idFilme == idFilme && a.idUsuario == idUsuario).FirstOrDefault();
        }

        private int PrimaryKeyGenero(string nome)
        {
           var genero = _context.Genero.Where(a => a.nome == nome).FirstOrDefault();

            if(genero == null)
            {
                genero = new Genero
                {
                    nome = nome
                };

                _context.Genero.Add(genero);
                _context.SaveChanges();
            }

            return genero.id;
        }

        private int PrimaryKeyArtista(string nome)
        {
            var artista = _context.Artista.Where(a => a.nome == nome).FirstOrDefault();

            if (artista == null)
            {
                artista = new Artista
                {
                    nome = nome
                };

                _context.Artista.Add(artista);
                _context.SaveChanges();
            }

            return artista.id;
        }

    }
}
