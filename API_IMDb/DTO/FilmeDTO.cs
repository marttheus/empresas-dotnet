﻿using API_IMDb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_IMDb.DTO
{
    public class FilmeDTO
    {
        public class Cadastro
        {
            public string nome { get; set; }
            public string diretor { get; set; }
            public string[] atores { get; set; }
            public string genero { get; set; }
            public DateTime ano_lancamento { get; set; }
        }

        public class Retorno
        {
            public int id { get; set; }
            public string nome { get; set; }
            public string diretor { get; set; }
            public string[] atores { get; set; }
            public string genero { get; set; }
            public DateTime? ano_lancamento { get; set; }

            public Retorno(Filme filme) 
            {
                id = filme.id;
                nome = filme.nome;
                diretor = filme.Diretor.nome;
                atores = filme.ArtistaFilmes.Select(s => s.Artista.nome).ToArray();
                genero = filme.Genero.nome;
                ano_lancamento = filme.ano_lancamento;
            }
        }

        public class Datalhes
        {
            public int id { get; set; }
            public string nome { get; set; }
            public string diretor { get; set; }
            public string[] atores { get; set; }
            public string genero { get; set; }
            public DateTime? ano_lancamento { get; set; }
            public double nota { get; set; }

            public Datalhes(Filme filme)
            {
                id = filme.id;
                nome = filme.nome;
                diretor = filme.Diretor.nome;
                atores = filme.ArtistaFilmes.Select(s => s.Artista.nome).ToArray();
                genero = filme.Genero.nome;
                ano_lancamento = filme.ano_lancamento;
                nota = filme.Votos.Select(s => s.voto).Average();
            }
        }
    }
}
