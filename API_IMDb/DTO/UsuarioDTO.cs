﻿using API_IMDb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace API_IMDb.DTO
{
    public class UsuarioDTO
    {
        public class Login
        {
            [Required]
            [EmailAddress]
            public string email { get; set; }
        }

        public class Cadastro
        {
            [Required]
            [EmailAddress]
            public string email { get; set; }
            [Required]
            public string nome { get; set; }
            [Required]
            [MinLength(6)]
            public string senha { get; set; }
        }

        public class Edicao
        {
            [Required]
            public string nome { get; set; }
            [Required]
            [MinLength(6)]
            public string senha { get; set; }
        }

        public class Retorno
        {
            public int id { get; set; }
            public string email { get; set; }
            public string nome { get; set; }

            public Retorno (Usuario usuario)
            {
                id = usuario.id;
                email = usuario.email;
                nome = usuario.nome;
            }
        }
    }
}
