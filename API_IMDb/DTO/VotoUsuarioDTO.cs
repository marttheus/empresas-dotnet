﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_IMDb.DTO
{
    public class VotoUsuarioDTO
    {
        public int idFilme { get; set; }
        [Range(1,4)]
        public int voto { get; set; }
    }
}
