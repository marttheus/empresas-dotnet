﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_IMDb.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Artista",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(nullable: false),
                    ano_nascimento = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artista", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Genero",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genero", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    email = table.Column<string>(nullable: false),
                    nome = table.Column<string>(nullable: false),
                    senha = table.Column<string>(nullable: false),
                    ativo = table.Column<int>(nullable: false),
                    perfil = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Filme",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(nullable: false),
                    ano_lancamento = table.Column<DateTime>(nullable: true),
                    idDiretor = table.Column<int>(nullable: false),
                    idGenero = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filme", x => x.id);
                    table.ForeignKey(
                        name: "FK_Filme_Artista_idDiretor",
                        column: x => x.idDiretor,
                        principalTable: "Artista",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Filme_Genero_idGenero",
                        column: x => x.idGenero,
                        principalTable: "Genero",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ArtistaFilme",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idArtista = table.Column<int>(nullable: false),
                    idFilme = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArtistaFilme", x => x.id);
                    table.ForeignKey(
                        name: "FK_ArtistaFilme_Artista_idArtista",
                        column: x => x.idArtista,
                        principalTable: "Artista",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ArtistaFilme_Filme_idFilme",
                        column: x => x.idFilme,
                        principalTable: "Filme",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VotoUsuario",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idUsuario = table.Column<int>(nullable: false),
                    idFilme = table.Column<int>(nullable: false),
                    voto = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VotoUsuario", x => x.id);
                    table.ForeignKey(
                        name: "FK_VotoUsuario_Filme_idFilme",
                        column: x => x.idFilme,
                        principalTable: "Filme",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VotoUsuario_Usuario_idUsuario",
                        column: x => x.idUsuario,
                        principalTable: "Usuario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Artista",
                columns: new[] { "id", "ano_nascimento", "nome" },
                values: new object[,]
                {
                    { 1, new DateTime(1963, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Quentin Tarantino" },
                    { 2, new DateTime(1954, 2, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "John Travolta" },
                    { 3, new DateTime(1970, 4, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), "Uma Thurman" },
                    { 4, new DateTime(1948, 12, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Samuel L. Jackson" }
                });

            migrationBuilder.InsertData(
                table: "Genero",
                columns: new[] { "id", "nome" },
                values: new object[] { 1, "Crime" });

            migrationBuilder.InsertData(
                table: "Usuario",
                columns: new[] { "id", "ativo", "email", "nome", "perfil", "senha" },
                values: new object[,]
                {
                    { 1, 1, "adm@imdb.com", "Administrador", "administrador", "654321" },
                    { 2, 1, "usuario@imdb.com", "Padrão", "usuario", "123456" }
                });

            migrationBuilder.InsertData(
                table: "Filme",
                columns: new[] { "id", "ano_lancamento", "idDiretor", "idGenero", "nome" },
                values: new object[] { 1, new DateTime(1994, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, "Pulp Fiction" });

            migrationBuilder.InsertData(
                table: "ArtistaFilme",
                columns: new[] { "id", "idArtista", "idFilme" },
                values: new object[,]
                {
                    { 1, 2, 1 },
                    { 2, 3, 1 },
                    { 3, 4, 1 }
                });

            migrationBuilder.InsertData(
                table: "VotoUsuario",
                columns: new[] { "id", "idFilme", "idUsuario", "voto" },
                values: new object[,]
                {
                    { 1, 1, 2, 4 },
                    { 2, 1, 2, 3 },
                    { 3, 1, 2, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArtistaFilme_idArtista",
                table: "ArtistaFilme",
                column: "idArtista");

            migrationBuilder.CreateIndex(
                name: "IX_ArtistaFilme_idFilme",
                table: "ArtistaFilme",
                column: "idFilme");

            migrationBuilder.CreateIndex(
                name: "IX_Filme_idDiretor",
                table: "Filme",
                column: "idDiretor");

            migrationBuilder.CreateIndex(
                name: "IX_Filme_idGenero",
                table: "Filme",
                column: "idGenero");

            migrationBuilder.CreateIndex(
                name: "IX_VotoUsuario_idFilme",
                table: "VotoUsuario",
                column: "idFilme");

            migrationBuilder.CreateIndex(
                name: "IX_VotoUsuario_idUsuario",
                table: "VotoUsuario",
                column: "idUsuario");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArtistaFilme");

            migrationBuilder.DropTable(
                name: "VotoUsuario");

            migrationBuilder.DropTable(
                name: "Filme");

            migrationBuilder.DropTable(
                name: "Usuario");

            migrationBuilder.DropTable(
                name: "Artista");

            migrationBuilder.DropTable(
                name: "Genero");
        }
    }
}
